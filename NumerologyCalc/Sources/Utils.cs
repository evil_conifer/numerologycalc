﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumerologyCalc.Sources
{
    public static class Utils
    {

        public static int[] GetIntDigits(int i)
        {
            Stack<int> digits = new Stack<int>();

            for (; i > 0; i /= 10)
            {
                digits.Push(i % 10);
            }

            return digits.ToArray();
        }

    }
}
