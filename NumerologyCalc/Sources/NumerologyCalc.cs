﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumerologyCalc.Sources
{
    public sealed class NumerologyCalc
    {

        public static void Start()
        {
            Console.WriteLine("Enter your string below:");
            string input = Console.ReadLine();
            int numero = GetNumeroNumber(input);
            Console.WriteLine("The Numerology number for the string " + input + " is " + numero);
            Console.ReadKey();


        }

        public static int GetNumeroNumber(string input)
        {
            if (input.Length > 0 && input != null)
            {
                string sanit = input.ToLower();
                char[] chars = sanit.ToCharArray();
                NumeroTable table = new NumeroTable();

                int num = 0;

                foreach (char c in chars)
                {
                    num += table.GetNumberFromLetter(c);
                    int[] digits = Utils.GetIntDigits(num);

                    if (digits.Length > 1)
                    {
                        num = digits.Sum();
                    }
                }

                return num;

            }
            else
            {
                Console.WriteLine("Error: string input cannot be null.");
                throw new ArgumentNullException("Input cannot be null!");
            }
        }

    }
}
