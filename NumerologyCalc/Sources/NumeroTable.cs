﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumerologyCalc.Sources
{
    public class NumeroTable
    {

        private Dictionary<char, int> _numbers;

        public NumeroTable()
        {
            Numbers.Add('a', 1);
            Numbers.Add('b', 2);
            Numbers.Add('c', 3);
            Numbers.Add('d', 4);
            Numbers.Add('e', 5);
            Numbers.Add('f', 8);
            Numbers.Add('g', 3);
            Numbers.Add('h', 5);
            Numbers.Add('i', 1);
            Numbers.Add('j', 1);
            Numbers.Add('k', 2);
            Numbers.Add('l', 3);
            Numbers.Add('m', 4);
            Numbers.Add('o', 7);
            Numbers.Add('p', 8);
            Numbers.Add('q', 1);
            Numbers.Add('r', 2);
            Numbers.Add('s', 3);
            Numbers.Add('t', 4);
            Numbers.Add('u', 6);
            Numbers.Add('v', 6);
            Numbers.Add('w', 6);
            Numbers.Add('x', 6);
            Numbers.Add('y', 1);
            Numbers.Add('z', 7);
        }

        public Dictionary<char, int> Numbers
        {
            get
            {
                if (this._numbers == null)
                {
                    this._numbers = new Dictionary<char, int>();
                }

                return _numbers;
            }
        }

        public int GetNumberFromLetter(char letter)
        {
            int number = 0;

            if (Numbers.ContainsKey(letter))
            {
                number = Numbers[letter];
            }

            return number;
        }

    }
}
